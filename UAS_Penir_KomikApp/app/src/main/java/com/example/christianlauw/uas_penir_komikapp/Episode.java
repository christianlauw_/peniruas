package com.example.christianlauw.uas_penir_komikapp;

/**
 * Created by Christian Lauw on 11/27/2017.
 */

public class Episode {
    private int id;
    private String epName;
    private String url;
    private int countView;
    private int komikId;
    private int total_like;
    private Boolean curr_user_like;


    public Episode(int pId,String pEpName,String pUrl,int pCount,int pKomikId)
    {
        setId(pId);
        setEpName(pEpName);
        setUrl(pUrl);
        setCountView(pCount);
        setKomikId(pKomikId);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEpName() {
        return epName;
    }

    public void setEpName(String epName) {
        this.epName = epName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCountView() {
        return countView;
    }

    public void setCountView(int countView) {
        this.countView = countView;
    }

    public int getKomikId() {
        return komikId;
    }

    public void setKomikId(int komikId) {
        this.komikId = komikId;
    }

    public int getTotal_like() {
        return total_like;
    }

    public void setTotal_like(int total_like) {
        this.total_like = total_like;
    }

    public Boolean getCurr_user_like() {
        return curr_user_like;
    }

    public void setCurr_user_like(Boolean curr_user_like) {
        this.curr_user_like = curr_user_like;
    }

    @Override
    public String toString(){
        return "ID: " + this.getId() + ", Episode: " + this.getEpName() + ", URL: " + this.getUrl() + ", count_view: " + this.getCountView() + ", komik_id: " + this.getKomikId() + ", total_like: " + this.getTotal_like() + ", curr_user_like: " + this.getCurr_user_like();
    }
}
