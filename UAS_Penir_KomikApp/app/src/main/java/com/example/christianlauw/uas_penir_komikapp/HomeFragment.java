package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class HomeFragment extends Fragment {
    public static HomeFragment newInstance(){
        return new HomeFragment();
    }

    public HomeFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("masuk","onCreateView HomeFragment");
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        ViewPager vpImageSlider = (ViewPager)view.findViewById(R.id.viewPagerHomeFrag);
        setupImageSlider(vpImageSlider);
        ArrayList<KomikRow> komikRows = getArguments().getParcelableArrayList("allKom");
        HomeRowAdapter adapter = new HomeRowAdapter(komikRows,this.getContext(), (MainActivity) this.getActivity());
        RecyclerView vRec = (RecyclerView) view.findViewById(R.id.recViewHomeFrag);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false);
        vRec.setLayoutManager(llm);
        vRec.setHasFixedSize(true);
        vRec.setAdapter(adapter);

        return view;
    }

    public void setupImageSlider(ViewPager vp){
        ImageSliderAdapter adapter = new ImageSliderAdapter(getActivity().getSupportFragmentManager());
        ImageSliderFragment frag1 = new ImageSliderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("image", R.drawable.conan2);
        frag1.setArguments(bundle);
        adapter.addFragment(frag1);

        frag1 = new ImageSliderFragment();
        bundle = new Bundle();
        bundle.putInt("image", R.drawable.maret);
        frag1.setArguments(bundle);
        adapter.addFragment(frag1);

        frag1 = new ImageSliderFragment();
        bundle = new Bundle();
        bundle.putInt("image", R.drawable.penyihir);
        frag1.setArguments(bundle);
        adapter.addFragment(frag1);
//        adapter.addFragment(new ImageSliderFragment());
//        adapter.addFragment(new ImageSliderFragment());
//        adapter.addFragment(new ImageSliderFragment());
        vp.setAdapter(adapter);
    }
}
