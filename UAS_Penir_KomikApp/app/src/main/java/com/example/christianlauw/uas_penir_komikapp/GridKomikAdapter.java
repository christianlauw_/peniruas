package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Aldo on 26/11/2017.
 */

public class GridKomikAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Komik> komiks;

    public GridKomikAdapter( ArrayList<Komik> komiks, Context context) {
        this.mContext = context;
        this.komiks = komiks;
    }

    @Override
    public int getCount() {
        return komiks.size();
    }

    @Override
    public Object getItem(int i) {
        return komiks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return komiks.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.grid_komik_layout, null);

        ImageView imgCover = (ImageView)v.findViewById(R.id.imageViewCover);
        TextView txtJudul = (TextView)v.findViewById(R.id.txtJudul);
        TextView txtFav = (TextView)v.findViewById(R.id.txtFav);
        TextView txtView = (TextView)v.findViewById(R.id.txtView);
        TextView txtRate = (TextView)v.findViewById(R.id.txtRate);


        // Current Komik
        Komik komik = komiks.get(i);

        // TO DO, URL & DATA BELUM SIAP
        //new ImageDownloader(imgCover).execute("http://103.52.146.34/penir/penir12/penir12/komik/ " + komik.getId() + "/cover.jpg");
        //new ImageDownloader(imgCover).execute("http://103.52.146.34/penir/penir12/penir12/komik/ " + komik.getId() + "/cover.jpg");
        ImageDownloader img = new ImageDownloader(imgCover);
        img.execute("http://103.52.146.34/penir/penir12/penir12/komik/"+komik.getId()+"/cover.jpg");
//        new ImageDownloader(imgCover).execute("http://103.52.146.34/penir/penir12/penir12/komik/"+komik.getId()+"/cover.jpg");
//        new ImageDownloader(imgCover).execute("http://103.52.146.34/penir/penir12/penir12/komik/" + komik.getId() + "/cover.jpg");

        // END-OF TO DO


        txtJudul.setText(komik.getJudul());
        txtFav.setText(String.valueOf(komik.getJumFav()));
        txtView.setText(String.valueOf(komik.getJumView()));
        txtRate.setText(String.valueOf(komik.getRating()) + " / 5");
        //txtFav.setText(komik);

        return v;
    }
}
