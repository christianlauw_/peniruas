package com.example.christianlauw.uas_penir_komikapp;

import com.plumillonforge.android.chipview.Chip;

/**
 * Created by Aldo on 01/12/2017.
 */

public class Tag implements Chip {
    private String mName;
    private int id = 0;
    private int position;

    public Tag(String name, int id, int position) {
        this.mName = name;
        this.id = id;
        this.position = position;
    }

    public Tag(String name) {
        mName = name;
    }

    @Override
    public String getText() {
        return mName;
    }

    public int getId() {
        return id;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
