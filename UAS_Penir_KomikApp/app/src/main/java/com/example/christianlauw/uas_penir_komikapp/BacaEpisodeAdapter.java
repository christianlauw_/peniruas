package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Aldo on 28/11/2017.
 */

public class BacaEpisodeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Integer> komikList;
    Integer id,episode;
    Context con;

    public BacaEpisodeAdapter(Context pCon,List<Integer> komikList, Integer id, Integer episode) {
        this.komikList = komikList;
        this.id = id;
        this.episode = episode;
        con = pCon;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.komik_recycle,parent,false);

        RecyclerView.ViewHolder vhold = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return vhold;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ImageView img = (ImageView) holder.itemView.findViewById(R.id.komik_image);
        //img.setImageBitmap(komikList.get(position));
//Tes
//        ImageDownloader imageDownloader = new ImageDownloader(img);
//        Toast.makeText(con, "id: " + id + ", episode: " + episode + ", gbr: " + komikList.get(position).toString(), Toast.LENGTH_SHORT).show();
//        imageDownloader.execute("http://103.52.146.34/penir/penir12/penir12/komik/"+id+"/"+episode+"/"+komikList.get(position).toString()+".jpg");
        URL url = null;
        try {
            url = new URL("http://103.52.146.34/penir/penir12/penir12/komik/"+id+"/"+episode+"/"+komikList.get(position).toString()+".jpg");
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            img.setImageBitmap(bmp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return komikList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
