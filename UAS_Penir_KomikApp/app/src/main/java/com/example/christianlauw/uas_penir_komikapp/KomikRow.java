package com.example.christianlauw.uas_penir_komikapp;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Christian Lauw on 11/24/2017.
 */

public class KomikRow implements Parcelable {

    //Untuk Tampilkan Baris, ex: Hot, Most Viewed
    private String rowName;
    private ArrayList<Komik> allKomik;

    public KomikRow(String pRowName,ArrayList<Komik> pAllKomik)
    {
        rowName = pRowName;
        allKomik = new ArrayList<>();
        allKomik = pAllKomik;
    }

    protected KomikRow(Parcel in) {
        rowName = in.readString();
    }

    public static final Creator<KomikRow> CREATOR = new Creator<KomikRow>() {
        @Override
        public KomikRow createFromParcel(Parcel in) {
            return new KomikRow(in);
        }

        @Override
        public KomikRow[] newArray(int size) {
            return new KomikRow[size];
        }
    };

    public String getRowName() {
        return rowName;
    }

    public void setRowName(String rowName) {
        this.rowName = rowName;
    }

    public ArrayList<Komik> getAllKomik() {
        return allKomik;
    }

    public void setAllKomik(ArrayList<Komik> allKomik) {
        this.allKomik = allKomik;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(rowName);
    }
}
