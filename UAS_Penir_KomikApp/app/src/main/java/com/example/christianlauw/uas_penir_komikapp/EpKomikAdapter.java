package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Christian Lauw on 11/27/2017.
 */

public class EpKomikAdapter extends ArrayAdapter<Episode> {
    public ArrayList<Episode> allEp;
    static Context con;
    static ImageButton imgLike;

    boolean[] status_like;
    public EpKomikAdapter(Context pCon,ArrayList<Episode> pEp) {
        super(pCon, R.layout.episode_list_layout,pEp);
        con = pCon;
        allEp = pEp;
        status_like = new boolean[allEp.size()];
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.episode_list_layout,parent,false);
        //Analogi file xml -> balon belum dipompa

        TextView tjudul = (TextView)v.findViewById(R.id.txtJudulEpisode);
        tjudul.setText(allEp.get(position).getEpName());

        TextView tCount = (TextView)v.findViewById(R.id.txtViewCount);
        tCount.setText(String.valueOf(allEp.get(position).getCountView()));

        TextView tLike = (TextView)v.findViewById(R.id.txtLikeCount);
        tLike.setText(String.valueOf(allEp.get(position).getTotal_like()));

//        String[] field={"user_id","episode_id"};
//        String[] value={Login.LOGIN_USERID,String.valueOf(allEp.get(position).getId())};
//        SendPostRequestAldo requestAldo = new SendPostRequestAldo("http://103.52.146.34/penir/penir12/penir12/scripts/user_liked_episode.php",field,value,this.getContext(),null,null,null,this);
//        requestAldo.execute();
        //v.setTag(allEp.get(position).getId());
        ImageView imgEp = (ImageView)v.findViewById(R.id.imgCoverEpisode);
        ImageDownloader imageDownloader = new ImageDownloader(imgEp);
        imageDownloader.execute("http://103.52.146.34/penir/penir12/penir12/komik/"+allEp.get(position).getKomikId()+"/"+allEp.get(position).getId()+"/1.jpg");

        /*imgLike = (ImageButton) v.findViewById(R.id.imgLikeEpisode);
        //imgLike.setTag(allEp.get(position).getId());
        Boolean currUserLike = allEp.get(position).getCurr_user_like();
        status_like[position] = currUserLike;
        if(currUserLike){
//            Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_black_24dp);
//            imgLike.setImageBitmap(bmp);
            imgLike.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
        }
        else{
//            Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_border_black_24dp);
//            imgLike.setImageBitmap(bmp);
            imgLike.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
        }
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String[] field={"user_id","komik_id","episode_id"};
//                String[] values={Login.LOGIN_USERID,String.valueOf(allEp.get(position).getKomikId()),String.valueOf(allEp.get(position).getId())};
//                if(status_like[position]){
//                    Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_black_24dp);
//                    imgLike.setImageBitmap(bmp);
//                    //imgLike.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
//                    SendPostRequest sr = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_unlike_ep.php",field,values,con,2);
//                    sr.execute();
//                    status_like[position] = false;
//                }
//                else{
//                    Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_border_black_24dp);
//                    imgLike.setImageBitmap(bmp);
//                    //imgLike.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
//                    SendPostRequest sr = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_like_ep.php",field,values,con,3);
//                    sr.execute();
//                    status_like[position] = true;
//                }
                changeImage(imgLike,position,allEp.get(position).getId(),allEp.get(position).getKomikId());
                Toast.makeText(con,allEp.get(position).getEpName(),Toast.LENGTH_SHORT).show();
            }
        });
        Toast.makeText(con, "ID : "+Login.LOGIN_USERID + ",Ep : "+String.valueOf(allEp.get(position).getId()), Toast.LENGTH_SHORT).show();*/
        CardView cv = (CardView)v.findViewById(R.id.crdEpLayout);
        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToBacaEpisode = new Intent(con,KomikActivity.class);
                goToBacaEpisode.putExtra("PILIH_EPISODE_KOMIK_EPISODE_ID",allEp.get(position).getId());
                String numEp = allEp.get(position).getEpName().substring(8);
                goToBacaEpisode.putExtra("PILIH_EPISODE_KOMIK_EP",numEp);
                goToBacaEpisode.putExtra("PILIH_EPISODE_KOMIK_KOMIK_ID",allEp.get(position).getKomikId());
                goToBacaEpisode.putExtra("EPISODE_NAME", allEp.get(position).getEpName());
                con.startActivity(goToBacaEpisode);
            }
        });
        return v;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
//    public static void postFinish(String result){
//        if(result.equals("1")){
//            //Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_black_24dp);
//            imgLike.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
//            Toast.makeText(con, "1", Toast.LENGTH_SHORT).show();
//        }
//        else{
//            //Bitmap bmp = BitmapFactory.decodeResource(con.getResources(),R.drawable.ic_favorite_border_black_24dp);
//            imgLike.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
//            Toast.makeText(con, "0", Toast.LENGTH_SHORT).show();
//        }
//    }

    /*public void changeImage(ImageButton imgButton,int pos,int epId,int pKomik){
        String[] field={"user_id","komik_id","episode_id"};
        String[] values={Login.LOGIN_USERID,String.valueOf(pKomik),String.valueOf(epId)};
        if(status_like[pos]){
            imgButton.setBackgroundResource(R.drawable.ic_favorite_black_24dp);
            SendPostRequest sr = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_unlike_ep.php",field,values,con,2);
            sr.execute();
            status_like[pos] = false;
        }
        else{
            imgButton.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp);
            SendPostRequest sr = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_like_ep.php",field,values,con,3);
            sr.execute();
            status_like[pos] = true;
        }
    }*/
}
