package com.example.christianlauw.uas_penir_komikapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText login_username,login_password;
    Button login_login,login_register;
    public static String LOGIN_USERNAME,LOGIN_USERID;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        login_username=(EditText)findViewById(R.id.login_username);
        login_password=(EditText)findViewById(R.id.login_password);
        login_login=(Button)findViewById(R.id.login_masuk);
        login_register=(Button)findViewById(R.id.login_register);

        login_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(login_username.getText().toString().equals("")){
                    Toast.makeText(Login.this,"Username harus diisi" , Toast.LENGTH_SHORT).show();
                }else if(login_password.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Password harus diisi", Toast.LENGTH_SHORT).show();
                }else if(login_password.getText().toString().equals("")&&login_username.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Username dan Password harus diisi", Toast.LENGTH_SHORT).show();
                }else{
                    try{
                        String[]field={"username","password"};
                        String[]values={login_username.getText().toString(),login_password.getText().toString()};
                        SendPostRequestAldo r = new SendPostRequestAldo("http://103.52.146.34/penir/penir12/penir12/scripts/user_login.php",field,values,Login.this, Login.this, null,null,null);
                        LOGIN_USERNAME=login_username.getText().toString();
                        r.execute();
//                    intent = new Intent(getApplicationContext(),MainActivity.class);
//                    startActivity(intent);
                    }catch (Exception e){
                        Toast.makeText(Login.this, "Eror:"+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        login_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(!login_username.getText().toString().equals("") || !login_password.getText().toString().equals("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setTitle("Konfirmasi");
                        builder.setMessage("Apakah anda yakin akan meninggalkan informasi diatas?");
                        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                intent=new Intent(Login.this,Register.class);
                                startActivity(intent);
                            }
                        });
                        builder.setNegativeButton("Tidak",null);
                        AlertDialog alert = builder.create();
                        alert.show();
//                        Toast.makeText(Login.this, "A", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        intent=new Intent(getApplicationContext(),Register.class);
                        startActivity(intent);
                    }
//                    intent=new Intent(getApplicationContext(),Register.class);
//                    startActivity(intent);
                }catch (Exception e){
                    Toast.makeText(Login.this, "Gagal Pindah : "+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void postFinish(String result){
        if(!result.equals("0")){
//            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            LOGIN_USERNAME=login_username.getText().toString();
            LOGIN_USERID=result;
            intent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(intent);

        }
        else{
            Toast.makeText(this, "Nope.", Toast.LENGTH_SHORT).show();
        }
    }
}
