package com.example.christianlauw.uas_penir_komikapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Aldo on 01/12/2017.
 */

public class Genre implements Parcelable{
    private int id;
    private String nama;
    private int position;

    public Genre(int id, String nama, int position){
        this.setId(id);
        this.setNama(nama);
        this.setPosition(position);
    }

    protected Genre(Parcel in) {
        setId(in.readInt());
        setNama(in.readString());
        setPosition(in.readInt());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getId());
        dest.writeString(getNama());
        dest.writeInt(getPosition());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Genre> CREATOR = new Creator<Genre>() {
        @Override
        public Genre createFromParcel(Parcel in) {
            return new Genre(in);
        }

        @Override
        public Genre[] newArray(int size) {
            return new Genre[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
