package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PilihEpisodeKomikActivity extends AppCompatActivity {

    Intent intentSelectedKomik;
    static int flag=0;
    public static ListView lv;
    static FloatingActionButton fabFav;
    static int statusFAB=0;
    static double rating;
    static double rateFrag =0;
    String komik_id;
    String overallRating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        setContentView(R.layout.activity_pilih_episode_komik);

        intentSelectedKomik=getIntent();
        overallRating = intentSelectedKomik.getStringExtra("rating");

//        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbarEP);
//        setSupportActionBar(toolbar);
        
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(intentSelectedKomik.getStringExtra("judul_komik"));
        }


        //Asumsi ambil idkomik dan idepisode

        lv = (ListView) findViewById(R.id.lstEpisode);
        String [] field={"user_id","komik_id"};
        String [] values={Login.LOGIN_USERID, intentSelectedKomik.getStringExtra("id_komik")}; //ambil dari intent
        //isi instance Ma nya waktu passing
        komik_id = intentSelectedKomik.getStringExtra("id_komik");

        ImageView imgCov = (ImageView) findViewById(R.id.imgCoverKomik);

        ImageDownloader imageDownloader = new ImageDownloader(imgCov);
        imageDownloader.execute("http://103.52.146.34/penir/penir12/penir12/komik/"+intentSelectedKomik.getStringExtra("id_komik")+"/cover.jpg");
        try{
            //Ambil episode komik x dan status jika dia like ->SendPostRequest Flag=0

//            String [] field={"user_id","komik_id"};
//            String [] values={Login.LOGIN_USERID, intentSelectedKomik.getStringExtra("id_komik")}; //ambil dari intent
            //Toast.makeText(this, Login.LOGIN_USERID + " ," + intentSelectedKomik.getStringExtra("id_komik"), Toast.LENGTH_SHORT).show();


            SendPostRequest sd = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/all_episode_komik.php",field,values,this,flag);
            sd.execute();
        }
        catch (Exception e){
            Toast.makeText(this, "error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        fabFav = (FloatingActionButton) findViewById(R.id.fabFavorite);

        fabFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String [] field={"user_id","komik_id"};
                String [] values={Login.LOGIN_USERID, intentSelectedKomik.getStringExtra("id_komik")}; //ambil dari intent
                if(statusFAB==1) {
                    SendPostRequest sd = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_unfav_komik.php", field, values, getApplicationContext(), 4);
                    sd.execute();
                    Snackbar.make(view, "Deleted from my favorites", Snackbar.LENGTH_SHORT).show();
                }
                else {
                    SendPostRequest sd = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_fav_komik.php", field, values, getApplicationContext(), 4);
                    sd.execute();
                    Snackbar.make(view, "Added to my favorites", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }
    public static void setFabStatus(String res)
    {
        Log.d("FAB FAB",res);
        if(res.equals("1"))
        {
            fabFav.setImageResource(R.drawable.ic_favorite_black_24dp);
            statusFAB=1;
//            Snackbar.make(R.id.coordinatorLayout, "Added to my favorites", Snackbar.LENGTH_SHORT);
        }
        else
        {
            Log.d("FAB FAB 2",res);
            fabFav.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            statusFAB=0;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.komik_act_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.rate) {
            FragmentManager fm = getSupportFragmentManager();
            RatingFragment ratingFragment = new RatingFragment();
            Bundle bundle = new Bundle();
            if(rateFrag != 0){
                bundle.putDouble("rating", rateFrag);
            }
            else{
                bundle.putDouble("rating", rating);
            }
            bundle.putString("overallRating", overallRating);
            bundle.putString("komik_id", komik_id);
            ratingFragment.setArguments(bundle);
            ratingFragment.show(fm, "ratingFragment");
        } else if(id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public static void getUserRating(double rate){
        rating = rate;
    }
}
