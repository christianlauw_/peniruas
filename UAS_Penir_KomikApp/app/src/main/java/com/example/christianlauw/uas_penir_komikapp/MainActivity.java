package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public ArrayList<KomikRow> allKom;
    public ArrayList<Komik> getAllKomik;
    public ArrayList<Fragment> fragments;


    public ViewPager  vp;
    public TabLayout tb;
    public NavigationView nv;
    public int flag=0;
    String[] isi={"Hot","Most Viewed","Most Favorite"};
    static ReadData rd;
    DrawerLayout dl;

    public ArrayList<Komik> hotKomiks;
    public ArrayList<Komik> mViewedKomiks;
    public ArrayList<Komik> mFavedKomiks;
    public ArrayList<Komik> mLikedKomiks;
    static public ArrayList<Komik> myFavorite;
    public ArrayList<Genre> genres;
    public ArrayList<Komik> selectAllKomik;

    // NEW CODE ADDED
    String []urls = {
            "http://103.52.146.34/penir/penir12/penir12/scripts/new_komik.php",
            "http://103.52.146.34/penir/penir12/penir12/scripts/most_viewed_komik.php",
            "http://103.52.146.34/penir/penir12/penir12/scripts/most_faved_komik.php",
            "http://103.52.146.34/penir/penir12/penir12/scripts/most_liked_komik.php",
            "http://103.52.146.34/penir/penir12/penir12/scripts/user_faved_komik.php",
            "http://103.52.146.34/penir/penir12/penir12/scripts/all_genre.php"};
    String[] arrays = {
            "new_komiks",
            "komiks_view",
            "komiks_fave",
            "komiks_like",
            "my_fav",
            "genres"}; //,
    int x = 0;
    static boolean refresh = false;
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_NO_TITLE);
////        getSupportActionBar().hide();
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEP);
        setSupportActionBar(toolbar);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

//        SharedPreferences sharedPref = this.getSharedPreferences("com.example.christianlauw.uas_penir_komikapp", Context.MODE_PRIVATE);
//        String username = sharedPref.getString("username", null);
//        String id = sharedPref.getString("id", null);
//        Toast.makeText(this, "Username: " + username + ", user_id: " + id, Toast.LENGTH_SHORT).show();

        Intent intent = getIntent();
        refresh = intent.getBooleanExtra("refresh", false);
        View v = findViewById(R.id.drawer);
        if(refresh){
            Snackbar.make(v, "Data has been refreshed!", Snackbar.LENGTH_LONG).show();
        }

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        dl = (DrawerLayout) findViewById(R.id.drawer);

        nv = (NavigationView) findViewById(R.id.nav_view);
        View headerView = nv.getHeaderView(0);
        TextView namauser = (TextView) headerView.findViewById(R.id.textViewUsername);
        namauser.setText(Login.LOGIN_USERNAME);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            // dummy
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                dl.closeDrawers();
                switch (item.getItemId()) {
                    case R.id.itemHome:
                        nv.getMenu().getItem(0).setChecked(true);
                        vp.setCurrentItem(0, true);
                        break;
                    case R.id.itemHot:
                        nv.getMenu().getItem(1).setChecked(true);
                        vp.setCurrentItem(1, true);
                        break;
                    case R.id.itemMostFav:
                        nv.getMenu().getItem(2).setChecked(true);
                        vp.setCurrentItem(2,true);
                        break;
                    case R.id.itemMostViewed:
                        nv.getMenu().getItem(3).setChecked(true);
                        vp.setCurrentItem(3,true);
                        break;
                    case R.id.idMostLike:
                        nv.getMenu().getItem(4).setChecked(true);
                        vp.setCurrentItem(4,true);
                        break;
                    case R.id.itemMyFav:
                        nv.getMenu().getItem(5).setChecked(true);
                        vp.setCurrentItem(5,true);
                        break;
                    case R.id.itemGenre:
                        nv.getMenu().getItem(6).setChecked(true);
                        vp.setCurrentItem(6,true);
                        break;
                }
                return true;
            }
        });

//        ViewPager vpImageSlider = (ViewPager)findViewById(R.id.viewPagerHome);
//        setupImageSlider(vpImageSlider);

        allKom =new ArrayList<>();
        fragments = new ArrayList<>();
        vp = (ViewPager) findViewById(R.id.viewpager);
        tb = (TabLayout) findViewById(R.id.tabsContent); //Root dari Tab
        hotKomiks = new ArrayList<>();
        mViewedKomiks = new ArrayList<>();
        mFavedKomiks = new ArrayList<>();
        selectAllKomik = new ArrayList<>();

        mLikedKomiks = new ArrayList<>();
        myFavorite = new ArrayList<>();

//        HomeFragment hm = new HomeFragment();
//        fragments.add(hm);
//        setupViewPager(vp, fragments);

        tb.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try{
                    vp.setCurrentItem(tab.getPosition());
                    nv.getMenu().getItem(tab.getPosition()).setChecked(true);
                }
                catch (Exception e){
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //vp.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tbNew = tb.getTabAt(position);
                tbNew.select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabRefresh);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition( 0, 0);
                startActivity(getIntent().putExtra("refresh", true));
                overridePendingTransition( 0, 0);

            }
        });
        try{
            createDummyData();

        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    public void createDummyData() {
        rd = new ReadData(this);
        if(x==4)
        {
            String[] field={"user_id"};
            String[] value={Login.LOGIN_USERID};
            SendPostRequest sd = new SendPostRequest(urls[x],field,value,this,8);
            sd.execute();
            x++;
            createDummyData();
        }
        else
            rd.execute(urls[x]);
    }

    public void readDataFinish(Context con, String res)
    {
        try {
            JSONObject json = new JSONObject(res);
            JSONArray json2;
            json2 = json.getJSONArray(arrays[x]);

            if(json2!=null) {
                Log.d("json", json2.toString());
                if(x>=0 && x<=3){
                    if(x!=3) //x==3 Untuk Ambil MostLiked
                        getAllKomik = new ArrayList<Komik>();
                    for (int i = 0; i < json2.length(); i++) {
                        JSONObject c = json2.getJSONObject(i);
                        Komik k = new Komik(c.getInt("id"),c.getString("nama"),"test");
                        k.setJumFav(c.getInt("jumFav"));
                        k.setJumView(c.getInt("jumView"));
                        k.setRating(c.getDouble("rating"));
                        k.setGenre_id(c.getInt("genre_id"));

                        if(x!=3)
                            getAllKomik.add(k);
//                            selectAllKomik.add(k);
                        switch(x){
                            case 0:
                                if(i<5){
                                    hotKomiks.add(k);
                                }
                                selectAllKomik.add(k);
                                break;
                            case 1:
                                mViewedKomiks.add(k);
                                break;
                            case 2:
                                mFavedKomiks.add(k);
                                break;
                            case 3:
                                mLikedKomiks.add(k);
                                Log.d("Masuk Add MostLiked","Horay part2");
                                break;
                        }
                    }
                    if(x!=3) {
                        KomikRow dm = new KomikRow(isi[x], getAllKomik);
                        allKom.add(dm);
                    }
                }
                else if(x==5){
                    genres = new ArrayList<>();
//                    Log.d("masuk","x=3");
                    for (int i = 0; i < json2.length(); i++) {
                        JSONObject c = json2.getJSONObject(i);
                        Genre g = new Genre(c.getInt("id"),c.getString("nama"), i);
                        genres.add(g);
                    }
                }

                if(x<5){
                    x++;
                    createDummyData();
                }
                else {
                    Log.d("Start","Masuk");
                    // DATA HOME SELESAI DI-FETCH,DISPLAY DI VIEW
                    HomeFragment homeFragment = new HomeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("allKom", allKom);
                    homeFragment.setArguments(bundle);
                    fragments.add(homeFragment);

                    // Data HOT
                    GridKomikFragment gridKomikFragment = new GridKomikFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", hotKomiks);
                    gridKomikFragment.setArguments(bundle);
                    fragments.add(gridKomikFragment);

                    // Data most Fav
                    gridKomikFragment = new GridKomikFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", mFavedKomiks);
                    gridKomikFragment.setArguments(bundle);
                    fragments.add(gridKomikFragment);

                    // Data most viewed
                    gridKomikFragment = new GridKomikFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", mViewedKomiks);
                    gridKomikFragment.setArguments(bundle);
                    fragments.add(gridKomikFragment);

                    //Data most liked
                    gridKomikFragment = new GridKomikFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", mLikedKomiks);
                    gridKomikFragment.setArguments(bundle);
                    fragments.add(gridKomikFragment);
//
//                    //Data My Favorite
                    Log.d("Dari SPost",String.valueOf(myFavorite.size()));
                    gridKomikFragment = new GridKomikFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", myFavorite);
                    gridKomikFragment.setArguments(bundle);
                    fragments.add(gridKomikFragment);

                    // Data genre
                    GenreFragment genreFragment = new GenreFragment();
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("genres", genres);
//                    String genre = "genre_";
//                    int counter = 0;
//                    for(Genre g : genres){
//                        List<Komik> listKomikGenre = new ArrayList<>();
//                        listKomikGenre = getKomikByGenre()
//                    }

                    bundle.putParcelableArrayList("komiks", selectAllKomik);
                    genreFragment.setArguments(bundle);
                    fragments.add(genreFragment);

                    setupViewPager(vp, fragments);
                }
            }
            else{
                Toast.makeText(con, "JSON2 null", Toast.LENGTH_LONG).show();
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(con,"Gangguan",Toast.LENGTH_SHORT).show();
            Toast.makeText(con, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void setupViewPager(ViewPager viewPager, ArrayList<Fragment> fragments){
        FragmentPageAdapt adapter = new FragmentPageAdapt(getSupportFragmentManager());
        for(Fragment f : fragments){
            adapter.addFragment(f);
            Log.d("setup:",f.toString());
        }
        Log.d("Masuk","masuk setViewPager");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            dl.openDrawer(GravityCompat.START);
        } else if(id == R.id.app_bar_search){
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<Komik> getKomikByGenre(ArrayList<Komik> allKomiks, int genre_id){
        ArrayList<Komik> listKomikByGenre = new ArrayList<>();
        for(Komik k : allKomiks){
            if(k.getGenre_id() == genre_id){
                listKomikByGenre.add(k);
            }
        }

        return listKomikByGenre;
    }

    public void replaceFragment(ViewPagerGenreFragment fragment){
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.container, fragment);
        t.addToBackStack(null);
        t.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

//    public void setupImageSlider(ViewPager vp){
//        ImageSliderAdapter adapter = new ImageSliderAdapter(getSupportFragmentManager());
//        adapter.addFragment(new ImageSliderFragment());
//        adapter.addFragment(new ImageSliderFragment());
//        adapter.addFragment(new ImageSliderFragment());
//        vp.setAdapter(adapter);
//    }

}
