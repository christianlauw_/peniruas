package com.example.christianlauw.uas_penir_komikapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Christian Lauw on 11/24/2017.
 */

public class Komik implements Parcelable{
    private int id;
    private String judul;
    private String urlCover;
    private int jumFav;
    private int jumView;
    private double rating;
    private int genre_id;

    public Komik(int pId,String pJudul,String pUrl)
    {
        setId(pId);
        setJudul(pJudul);
        setUrlCover(pUrl);
    }

    protected Komik(Parcel in) {
        id = in.readInt();
        judul = in.readString();
        urlCover = in.readString();
    }


    public static final Creator<Komik> CREATOR = new Creator<Komik>() {
        @Override
        public Komik createFromParcel(Parcel in) {
            return new Komik(in);
        }

        @Override
        public Komik[] newArray(int size) {
            return new Komik[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getUrlCover() {
        return urlCover;
    }

    public void setUrlCover(String urlCover) {
        this.urlCover = urlCover;
    }


    public int getJumFav() {
        return jumFav;
    }

    public void setJumFav(int jumFav) {
        this.jumFav = jumFav;
    }

    public int getJumView() {
        return jumView;
    }

    public void setJumView(int jumView) {
        this.jumView = jumView;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }



    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(judul);
        parcel.writeString(urlCover);
        parcel.writeInt(jumFav);
        parcel.writeInt(jumView);
        parcel.writeDouble(rating);
        parcel.writeInt(genre_id);
    }
}
