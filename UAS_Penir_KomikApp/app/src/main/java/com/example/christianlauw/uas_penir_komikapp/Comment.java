package com.example.christianlauw.uas_penir_komikapp;

/**
 * Created by Lazuardi on 12/5/2017.
 */

public class Comment {
    private int id, user_id, episode_id;
    private String username;
    private String isi;

    public Comment(int id, int user_id, int episode_id, String isi, String username) {
        this.id = id;
        this.user_id = user_id;
        this.episode_id = episode_id;
        this.isi = isi;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getEpisode_id() {
        return episode_id;
    }

    public void setEpisode_id(int episode_id) {
        this.episode_id = episode_id;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }
}
