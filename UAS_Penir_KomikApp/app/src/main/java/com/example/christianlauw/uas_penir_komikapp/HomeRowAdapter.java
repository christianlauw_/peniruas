package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Christian Lauw on 11/24/2017.
 */

public class HomeRowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<KomikRow> allKomikRow;
    ArrayList<ProductRow> allProductRow;
    Context con;
    MainActivity ma;

    public HomeRowAdapter(ArrayList<KomikRow> pAllKomikRow,Context pCon, MainActivity ma)
    {
        allKomikRow = new ArrayList<>();
        allKomikRow = pAllKomikRow;
        con = pCon;
        this.ma = ma;
    }
//    public HomeRowAdapter(ArrayList<ProductRow> pAllProduct,Context pCon)
//    {
//        allProductRow = new ArrayList<>();
//        allProductRow = pAllProduct;
//        con = pCon;
//    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate Row
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_recycleview,null);
        RecyclerView.ViewHolder vHold = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return vHold;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final String rowName = allKomikRow.get(position).getRowName();
        ArrayList<Komik> arrKomik = allKomikRow.get(position).getAllKomik();

//        String rowName = allProductRow.get(position).getJudul();
//        ArrayList<Product> arrProd = allProductRow.get(position).getAllProd();

        TextView txtTitle = (TextView) holder.itemView.findViewById(R.id.txtContent);
        txtTitle.setText(rowName);

        Button btnShow = (Button)holder.itemView.findViewById(R.id.btnShow);
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(con, rowName, Toast.LENGTH_SHORT).show();
                if(rowName.equals("Hot")){
                    ma.vp.setCurrentItem(1, true);
                } else if(rowName.equals("Most Viewed")){
                    ma.vp.setCurrentItem(3, true);
                } else if(rowName.equals("Most Favorite")){
                    ma.vp.setCurrentItem(2, true);
                }
            }
        });

        RecycleViewHomeAdapter adapter = new RecycleViewHomeAdapter(con,arrKomik);
        //RecycleViewHomeAdapter adapter = new RecycleViewHomeAdapter(con,arrProd);
        RecyclerView rv = (RecyclerView) holder.itemView.findViewById(R.id.recView);
        LinearLayoutManager llm = new LinearLayoutManager(con,LinearLayoutManager.HORIZONTAL,false);

        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return allKomikRow.size();
    }
}
