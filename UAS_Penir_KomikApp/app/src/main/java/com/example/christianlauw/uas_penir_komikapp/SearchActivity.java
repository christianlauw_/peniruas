package com.example.christianlauw.uas_penir_komikapp;

import android.os.StrictMode;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SearchActivity extends AppCompatActivity {
    EditText txtSearch;
    static Integer flag = 10;
    static LinearLayout layoutContainer;
    static FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        fm = getSupportFragmentManager();

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Search Keyword");
        }

        txtSearch = (EditText)findViewById(R.id.txtSearch);
        layoutContainer = (LinearLayout)findViewById(R.id.linearLayoutContainer);

//        final FragmentManager fm = getSupportFragmentManager();
        Button btnSearch = (Button)findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                FragmentTransaction trans = fm.beginTransaction();
                //Toast.makeText(SearchActivity.this, "masuk sini", Toast.LENGTH_SHORT).show();
                String[] field = {"query"};
                String[] value = {txtSearch.getText().toString()};
                SendPostRequest request = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/search_komik.php", field, value,getApplicationContext(), flag);
                request.execute();
            }
        });

        Button btnShowAll = (Button)findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] field = {"query"};
                String[] value = {""};
                SendPostRequest request = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/search_komik.php", field, value,getApplicationContext(), flag);
                request.execute();
            }
        });
    }

    public static void replaceFragment(GridKomikFragment fragment){
        FragmentTransaction t = fm.beginTransaction();
        t.replace(R.id.linearLayoutContainer, fragment);
        t.addToBackStack(null);
        t.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }
}
