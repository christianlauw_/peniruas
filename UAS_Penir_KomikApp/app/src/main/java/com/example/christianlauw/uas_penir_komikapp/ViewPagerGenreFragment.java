package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ViewPagerGenreFragment extends Fragment {

    GridKomikAdapter adapter;
    public ViewPagerGenreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_view_pager_genre, container, false);

        // ambil data nama genre
        String nama_genre = getArguments().getString("nama_genre");
        ArrayList<Komik> komiks = getArguments().getParcelableArrayList("komiks");

        TextView t = v.findViewById(R.id.txtGenre);
        t.setText(nama_genre);

        GridView g = v.findViewById(R.id.gridGenre);
        adapter = new GridKomikAdapter(komiks, this.getContext());
        g.setAdapter(adapter);
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                Intent intent = new Intent(getContext(),PilihEpisodeKomikActivity.class);
                intent.putExtra("judul_komik", ((Komik)adapter.getItem(i)).getJudul());
                intent.putExtra("id_komik",String.valueOf(id));
                //Toast.makeText(con, txtID.getText().toString(), Toast.LENGTH_SHORT).show();
                getContext().startActivity(intent);
                //Toast.makeText(getContext(), String.valueOf(id), Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }
}
