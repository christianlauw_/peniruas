package com.example.christianlauw.uas_penir_komikapp;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class RatingFragment extends DialogFragment {
    static Integer flag = 7;
    public RatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_rating, container, false);
        double rating = getArguments().getDouble("rating");
        String overallRate = getArguments().getString("overallRating");
        String txtViewRateText = "Overall rating: " + overallRate + " / 5";
        final String komik_id = getArguments().getString("komik_id");
        final RatingBar rbKomik = (RatingBar)v.findViewById(R.id.ratingBarKomik);
        final TextView txtOverallRating = (TextView)v.findViewById(R.id.txtOverall);
        txtOverallRating.setText(txtViewRateText);
        rbKomik.setRating((float)rating);


        Button btnRate = (Button)v.findViewById(R.id.buttonRate);
        btnRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rbKomik.getRating() > 0){
                    String[] field = {"user_id","komik_id","rating"};
                    String[] values = {Login.LOGIN_USERID, komik_id, String.valueOf(rbKomik.getRating())};
                    ///Log.d("tes RatingFragment", Login.LOGIN_USERID + "," + komik_id + "," + String.valueOf(rbKomik.getRating()));
                    SendPostRequest r = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_rate_komik.php", field, values, getContext(), flag);
                    r.execute();
                    Toast.makeText(v.getContext(), "Your rating has been submitted.", Toast.LENGTH_SHORT).show();
                    PilihEpisodeKomikActivity.rateFrag = (double)rbKomik.getRating();
                    dismiss();
                }
                else{
                    Toast.makeText(v.getContext(), "Sorry, you cannot rate 0/5.", Toast.LENGTH_SHORT).show();
                }
                //Snackbar.make(view, "Your rating has been submited", Snackbar.LENGTH_LONG).show();
            }
        });
        return v;
    }

}
