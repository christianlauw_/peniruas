package com.example.christianlauw.uas_penir_komikapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ImageSliderFragment extends Fragment {


    public ImageSliderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_image_slider, container, false);
        ImageView img = (ImageView)v.findViewById(R.id.imgViewSlider);
        int image = getArguments().getInt("image");
        img.setImageResource(image);
        return v;
    }

}
