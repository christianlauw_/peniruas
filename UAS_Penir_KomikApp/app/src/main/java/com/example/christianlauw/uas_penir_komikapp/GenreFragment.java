package com.example.christianlauw.uas_penir_komikapp;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.plumillonforge.android.chipview.Chip;
import com.plumillonforge.android.chipview.ChipView;
import com.plumillonforge.android.chipview.OnChipClickListener;

import java.util.ArrayList;
import java.util.List;

import static com.example.christianlauw.uas_penir_komikapp.R.color.colorPrimary;


/**
 * A simple {@link Fragment} subclass.
 */
public class GenreFragment extends Fragment {

    List<Komik> komiks;
    List<Fragment> fragments;
    static ArrayList<ArrayList<Komik>> listGenreKomik;
    public GenreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_genre, container, false);

        // ambil genre
        List<Genre> genres = getArguments().getParcelableArrayList("genres");

        // ambil all komik
        komiks = getArguments().getParcelableArrayList("komiks");
//        Log.d("GenreFragment", komiks.toString());

        // declare arrlist fragment
        fragments = new ArrayList<>();

        // declare
        listGenreKomik = new ArrayList<ArrayList<Komik>>();

        // setting chiplist
        List<Chip> chipList = new ArrayList<>();
        int x = 0;
//        final ViewPagerGenreFragment viewPagerGenreFragment = new ViewPagerGenreFragment();
        for(Genre genre : genres) {
            chipList.add(new Tag(genre.getNama().toUpperCase(), genre.getId(), x));
            x++;
//
//            // setting fragment untuk viewpager
//            viewPagerGenreFragment = new ViewPagerGenreFragment();


            ArrayList<Komik> listKomik = getKomikByGenre(genre.getId());
            listGenreKomik.add(listKomik);


//            try{
//                ViewPagerGenreFragment viewPagerGenreFragment = new ViewPagerGenreFragment();
//                Bundle bundle = new Bundle();
//                bundle.putParcelableArrayList("komiks", listKomik);
//                bundle.putString("nama_genre", genre.getNama());
//                viewPagerGenreFragment.setArguments(bundle);
//                fragments.add(viewPagerGenreFragment);
//            }
//            catch(Exception e){
//                Toast.makeText(this.getContext(), "error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
//            }


        }

        // default position 0
        ViewPagerGenreFragment viewPagerGenreFragment = new ViewPagerGenreFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("komiks", listGenreKomik.get(0));
        bundle.putString("nama_genre", "SLICE OF LIFE");
        viewPagerGenreFragment.setArguments(bundle);
        ((MainActivity)getActivity()).replaceFragment(viewPagerGenreFragment);

//                    trans.replace(R.id.container, viewPagerGenreFragment);
//                    trans.addToBackStack(null);
//                    trans.commit();


        // setup view pager
//        final ViewPager vp = (ViewPager)v.findViewById(R.id.viewpagerGenre);
//        final View touchView = vp;
//        touchView.setOnTouchListener(new View.OnTouchListener()
//        {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return false;
//            }
//        });
//        setupViewPagerGenre(vp, fragments);

        // tampilkan menu
        ChipView chipDefault = (ChipView) v.findViewById(R.id.chipview);
        chipDefault.setChipSidePadding(20);
        chipDefault.setChipPadding(10);
        //chipDefault.setChipBackgroundColor(getResources().getColor(R.color.colorAccent));
        chipDefault.setChipBackgroundRes(R.drawable.untuk_button);
        chipDefault.setChipList(chipList);



//        final FragmentManager fm = this.getActivity().getSupportFragmentManager();
//        fragments.add(viewPagerGenreFragment);
//        trans.add(R.id.container, viewPagerGenreFragment);
//        trans.commit();


        chipDefault.setOnChipClickListener(new OnChipClickListener() {
            @Override
            public void onChipClick(Chip chip) {
//                vp.setCurrentItem(((Tag)chip).getPosition());
                try{

//                    FragmentTransaction trans = fm.beginTransaction();
                    ViewPagerGenreFragment viewPagerGenreFragment = new ViewPagerGenreFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", listGenreKomik.get(((Tag)chip).getPosition()));
                    bundle.putString("nama_genre", chip.getText());
                    viewPagerGenreFragment.setArguments(bundle);

//                    trans.replace(R.id.container, viewPagerGenreFragment);
//                    trans.addToBackStack(null);
//                    trans.commit();
                    ((MainActivity)getActivity()).replaceFragment(viewPagerGenreFragment);
                    //Toast.makeText(v.getContext(), chip.getText(), Toast.LENGTH_SHORT).show();
                }
                catch (Exception e){
                    Toast.makeText(v.getContext(), "error" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button btnGenrePage = (Button)v.findViewById(R.id.btnGenrePage);
        btnGenrePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GenreActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }
//
    // method setup view pager berdasarkan list fragment
    public void setupViewPagerGenre(ViewPager vp, List<Fragment> fragments){
        FragmentPageAdapt adapter = new FragmentPageAdapt(this.getFragmentManager());
        for(Fragment f : fragments){
            adapter.addFragment(f);
        }
        vp.setAdapter(adapter);
    }


    // ini bikin error
    public ArrayList<Komik> getKomikByGenre(int genre_id){
        ArrayList<Komik> komikGenre = new ArrayList<>();
        for(Komik k : komiks){
            if(k.getGenre_id() == genre_id){
                komikGenre.add(k);
            }
            Log.d("getKomikByGenre", "k.getGenre_id(): " + k.getGenre_id() + ", genre_id: " + genre_id);
        }
        Log.d("GenreFragment", komiks.toString());
        return komikGenre;
    }
}
