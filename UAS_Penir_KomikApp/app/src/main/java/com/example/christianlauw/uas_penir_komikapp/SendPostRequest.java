package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Laptop-SI on 13/10/2017.
 */

public class SendPostRequest extends AsyncTask<String, Void, String> {
    String sUrl;			// untuk URL yang dituju
    String []field;		// nama-nama field pd tabel DB
    String []values;		// value yg akan disimpan ke DB
    Context context;		// object context dari activity
    int flag;
    String numEp;

    protected void onPreExecute(){}

    //Untuk mengakali db supaya ep tetap bisa diakses foldernya
    public SendPostRequest(String sUrl, String[] field, String[] values, Context context,int pFlag,String pNumEp) {
        this.sUrl = sUrl;
        this.field = field;
        this.values = values;
        this.context = context;
        flag = pFlag;
        numEp = pNumEp;
    }

    public SendPostRequest(String sUrl, String[] field, String[] values, Context context,int pFlag) {
        this.sUrl = sUrl;
        this.field = field;
        this.values = values;
        this.context = context;
        flag = pFlag;
    }

    protected String doInBackground(String... arg0) {
        try {

            URL url = new URL(sUrl);
            JSONObject postDataParams = new JSONObject();
            for(int i = 0; i < field.length; i++) {
                postDataParams.put(field[i], values[i]);
            }
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream()));
                StringBuffer sb = new StringBuffer("");
                String line="";

                while((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();

            }
            else {
                return new String("false : "+responseCode);
            }
        }
        catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.d("tes",result);
        Log.d("Flag Komik",String.valueOf(KomikActivity.flag));
        if(flag==0) //Get All Episode in komik
        {
            Log.d("Flag SPos","Flag Masuk");
            Log.d("Res",result);
            if(result!=null) {
                try {
                    JSONObject obj = new JSONObject(result);
                    JSONArray getObj = obj.getJSONArray("episodes");
                    ArrayList<Episode> allEp = new ArrayList<>();
                    Log.d("Flag SPos", "Flag Masuk Lagi");
                    for (int i = 0; i < getObj.length(); i++) {
                        JSONObject obj2 = getObj.getJSONObject(i);
                        //Toast.makeText(context, "obj: " + obj2.toString(), Toast.LENGTH_SHORT).show();
                        //Asumsi URL /komik/values[0]/obj2.getInt("id")/
                        Episode ep = new Episode(obj2.getInt("id"), obj2.getString("nama"), obj2.getString("url"), obj2.getInt("count_view"), obj2.getInt("komik_id"));

                        ep.setTotal_like(obj2.getInt("total_like"));
                        int like = obj2.getInt("user_like");
                        boolean user_like = false;
                        if(like==1){
                            user_like = true;
                        }
                        ep.setCurr_user_like(user_like);
//                        //Episode ep= new Ep();
                        allEp.add(ep);
//                        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, ep.toString(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, allEp.size(), Toast.LENGTH_SHORT).show();
                    }
                    EpKomikAdapter ep = new EpKomikAdapter(context, allEp);
                    PilihEpisodeKomikActivity.lv.setAdapter(ep);

                    String getFav = obj.getString("res");

                    JSONArray user_rating = obj.getJSONArray("user_rate");
                    double rating =0.0;
                    try{
                        rating = user_rating.getJSONObject(0).getDouble("rating");
                    }
                    catch (Exception e){
                        rating = 0.0;
                    }

                    Log.d("Get Fav",getFav);
                    PilihEpisodeKomikActivity.setFabStatus(getFav);
                    PilihEpisodeKomikActivity.getUserRating(rating);
                } catch (JSONException e) {
                    Log.d("Flag SPos", "Flag Gagal");
                    e.printStackTrace();
                }
            }
        }else if(flag==1){ //Mau Baca Episode Dari Komik tertentu
            Log.d("Komik Flag","Masuk");
            Log.d("res",result);
            if(result!=null) {
                try {
                    JSONObject obj = new JSONObject(result);
                    JSONArray getObj = obj.getJSONArray("jumGambar");
                    JSONObject count = getObj.getJSONObject(0);
                    Log.d("Episode komik", count.toString());
                    int jumlah = count.getInt("jumlahGambar");
//                    Toast.makeText(context,String.valueOf(jumlah),Toast.LENGTH_SHORT).show();
                    ArrayList<Integer> allGam = new ArrayList<>();
                    for (int i = 1; i <=jumlah; i++) {
                        allGam.add(i);
                    }
                    Log.d("item",String.valueOf(allGam.size()));
                    BacaEpisodeAdapter ep = new BacaEpisodeAdapter(context,allGam,Integer.parseInt(values[1]),Integer.parseInt(values[0]));
//
                    LinearLayoutManager llm = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
                    KomikActivity.recyclerView.setLayoutManager(llm);
                    KomikActivity.recyclerView.setHasFixedSize(true);
                    KomikActivity.recyclerView.setAdapter(ep);
                } catch (JSONException e) {
                    Log.d("Flag Baca Episode", "Flag Gagal");
                    e.printStackTrace();
                }
            }
        }else if(flag==2) {//Set FAB LIke di Komik Activity
            Log.d("Flag 2-3", result);
            KomikActivity.setFABLikeStatus(result);
        }
        else if(flag==4) // set FAB favorit
        {
            Log.d("Stat Result FAB",result);
            PilihEpisodeKomikActivity.setFabStatus(result);
        }
        else if(flag==5) // get comments of episode
        {
            if(result!=null) {
                //Toast.makeText(context, "masuk sini", Toast.LENGTH_SHORT).show();
                try {
                    JSONObject obj = new JSONObject(result);
                    JSONArray getObj = obj.getJSONArray("comments");
                    ArrayList<Comment> allComment = new ArrayList<>();
                    Log.d("Flag SPos", "Flag Masuk Lagi");
                    for (int i = 0; i < getObj.length(); i++) {
                        JSONObject obj2 = getObj.getJSONObject(i);
                        Comment comment = new Comment(obj2.getInt("id"), obj2.getInt("user_id"), obj2.getInt("episode_id"), obj2.getString("content"), obj2.getString("username"));
                        allComment.add(comment);
                    }
                    CommentListAdapter adapter = new CommentListAdapter(context, allComment);
                    LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    CommentActivity.rv.setLayoutManager(llm);
                    CommentActivity.rv.setHasFixedSize(true);
                    CommentActivity.rv.setAdapter(adapter);
                    CommentActivity.rv.scrollToPosition(allComment.size()-1);
                } catch (JSONException e) {
                    Log.d("Flag SPos", "Flag Gagal");

                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    Log.d("Toast Error", e.getMessage());
                }
            }
        }
        else if(flag == 6){ // submit a comment to an episode
            if(result!=null){
                if(result == "1"){
                    Snackbar.make(CommentActivity.cl, "Your comment has been submitted", Snackbar.LENGTH_SHORT).show();
                }
                else if(result=="0"){
                    Snackbar.make(CommentActivity.cl, "Oops, something strange occured", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
        else if(flag == 7){
            //Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
        }
        else if(flag == 10){
            try {
                Log.d("komik_search", result);
                JSONObject json = new JSONObject(result);
                JSONArray json2;

                json2 = json.getJSONArray("komiks");
                ArrayList<Komik> komiks = new ArrayList<>();

                if (json2 != null) {
                    for (int i = 0; i < json2.length(); i++) {
                        JSONObject c = json2.getJSONObject(i);
                        Komik k = new Komik(c.getInt("id"), c.getString("nama"), "test");
//                        JSONObject c = json2.getJSONObject(i);
                        k.setJumFav(c.getInt("jumFav"));
                        k.setJumView(c.getInt("jumView"));
                        k.setRating(c.getDouble("rating"));
                        k.setGenre_id(c.getInt("genre_id"));
                        komiks.add(k);
                    }


                    GridKomikFragment gridKomikFragment = new GridKomikFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("komiks", komiks);
                    gridKomikFragment.setArguments(bundle);
                    SearchActivity.replaceFragment(gridKomikFragment);


                    //FragmentTransaction trans = gridKomikFragment.getS
                }
            }
            catch (JSONException ex) {

                ex.printStackTrace();
                Toast.makeText(this.context,"Gangguan",Toast.LENGTH_SHORT).show();
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
        else if(flag==8) //DariMainActivity ->MyFavorite
        {
            Log.d("Masuk Flag 10",String.valueOf(flag));
            try {
                JSONObject json = new JSONObject(result);
                JSONArray json2 = json.getJSONArray("my_fav");
                for(int i=0;i<json2.length();i++)
                {
                    JSONObject c = json2.getJSONObject(i);
                    Komik k = new Komik(c.getInt("id"),c.getString("nama"),"test");
                    k.setJumFav(c.getInt("jumFav"));
                    k.setJumView(c.getInt("jumView"));
                    k.setRating(c.getDouble("rating"));
                    k.setGenre_id(c.getInt("genre_id"));
                    MainActivity.myFavorite.add(k);
                }
                Log.d("Selesai Terisi",String.valueOf(MainActivity.myFavorite.size()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

//        Toast.makeText(context, result,
//                Toast.LENGTH_LONG).show();
    }
    public void ReturnEpisodeList(String result)
    {
        if(result!=null) {
            try {
                JSONObject obj = new JSONObject(result);
                String res = obj.getString("res");
                if(res=="1") {
                    JSONArray getObj = obj.getJSONArray("episodes");
                    ArrayList<Episode> allEp = new ArrayList<>();
                    Log.d("Flag SPos", "Flag Masuk Lagi");
                    for (int i = 0; i < getObj.length(); i++) {
                        JSONObject obj2 = getObj.getJSONObject(i);
                        //Toast.makeText(context, "obj: " + obj2.toString(), Toast.LENGTH_SHORT).show();
                        //Asumsi URL /komik/values[0]/obj2.getInt("id")/
                        Episode ep = new Episode(obj2.getInt("id"), "Episode " + (i + 1), obj2.getString("url"), obj2.getInt("count_view"), Integer.parseInt(values[0]));
                        ep.setTotal_like(obj2.getInt("total_like"));
                        int like = obj2.getInt("user_like");
                        boolean user_like = false;
                        if (like == 1) {
                            user_like = true;
                        }
                        ep.setCurr_user_like(user_like);
//                        //Episode ep= new Ep();
                        allEp.add(ep);
//                        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, ep.toString(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, allEp.size(), Toast.LENGTH_SHORT).show();
                    }

                    EpKomikAdapter ep = new EpKomikAdapter(context, allEp);
                    PilihEpisodeKomikActivity.lv.setAdapter(ep);
                }
            } catch (JSONException e) {
                Log.d("Flag SPos", "Flag Gagal");
                e.printStackTrace();
            }
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}

