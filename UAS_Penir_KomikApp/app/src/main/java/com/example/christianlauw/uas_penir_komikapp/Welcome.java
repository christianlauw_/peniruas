package com.example.christianlauw.uas_penir_komikapp;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class Welcome extends AppCompatActivity {
    Intent intent;
    ConstraintLayout constraintLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);

        intent = new Intent(this,Login.class);
        constraintLayout = (ConstraintLayout)findViewById(R.id.welcome_constraint);
        constraintLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                try{
                    startActivity(intent);
                    return true;
                }catch (Exception e){
                    Toast.makeText(Welcome.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
    }
}
