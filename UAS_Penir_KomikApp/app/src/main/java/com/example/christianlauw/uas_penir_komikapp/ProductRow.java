package com.example.christianlauw.uas_penir_komikapp;

import java.util.ArrayList;

/**
 * Created by Christian Lauw on 11/25/2017.
 */

public class ProductRow {
    private ArrayList<Product> allProd;
    private String judul;
    public ProductRow(String pJudul,ArrayList<Product> pAllProd)
    {
        allProd = new ArrayList<>();
        setAllProd(pAllProd);
        setJudul(pJudul);
    }

    public ArrayList<Product> getAllProd() {
        return allProd;
    }

    public void setAllProd(ArrayList<Product> allProd) {
        this.allProd = allProd;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }
}
