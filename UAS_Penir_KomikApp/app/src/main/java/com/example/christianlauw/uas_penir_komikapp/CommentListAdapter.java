package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Lazuardi on 12/5/2017.
 */

public class CommentListAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    List<Comment> listComment;
    Context context;
    public CommentListAdapter(Context context, List<Comment> listComment) {
//        this.komikList = komikList;
//        this.id = id;
//        this.episode = episode;
//        con = pCon;
        this.context = context;
        this.listComment = listComment;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_comment_layout,parent,false);

        RecyclerView.ViewHolder vhold = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        return vhold;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        TextView txtUsername = (TextView)holder.itemView.findViewById(R.id.txtUserName);
        TextView txtContent = (TextView)holder.itemView.findViewById(R.id.txtCommentContent);

        txtUsername.setText(listComment.get(position).getUsername());
        txtContent.setText(listComment.get(position).getIsi());
    }

    public int getItemCount() {
        return listComment.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
