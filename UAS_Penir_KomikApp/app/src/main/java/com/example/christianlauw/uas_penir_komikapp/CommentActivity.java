package com.example.christianlauw.uas_penir_komikapp;

import android.content.Intent;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class CommentActivity extends AppCompatActivity {
    static int flag = 5;
    static RecyclerView rv;
    static ConstraintLayout cl;
    Intent intent;
    String episode_id;
    TextView txtContentComment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        rv = (RecyclerView) findViewById(R.id.recViewComment_);
        cl = (ConstraintLayout)findViewById(R.id.layoutActComment);
        intent = getIntent();
        episode_id=intent.getStringExtra("episode_id");

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        //Log.d("Id Episode",value);


//        Toast.makeText(this, values[0], Toast.LENGTH_SHORT).show();
    //    Log.d("commentAct", String.valueOf(getIntent().getIntExtra("episode_id",0)));
        getAllComment();

        txtContentComment = (TextView)findViewById(R.id.comment_content);

        Button btnSubmitComment = (Button)findViewById(R.id.comment_submit);
        btnSubmitComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = 6;
                String[]field={"content","episode_id","user_id"};
                String[]values={txtContentComment.getText().toString(),episode_id,Login.LOGIN_USERID};
                SendPostRequest sendPostRequestComment = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/insert_comment.php", field, values, CommentActivity.this, flag);
                sendPostRequestComment.execute();
                flag = 5;
                getAllComment();
                txtContentComment.setText("");

                Snackbar.make(view, "Your comment has been submitted", Snackbar.LENGTH_LONG).setAction("Continue Reading", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                }).show();
            }
        });
    }

    public void getAllComment(){
        String[] field = {"episode_id"};
        String[] values = {episode_id};
        SendPostRequest sendPostRequest = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/get_comment.php", field, values, CommentActivity.this, flag);
        sendPostRequest.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }
}
