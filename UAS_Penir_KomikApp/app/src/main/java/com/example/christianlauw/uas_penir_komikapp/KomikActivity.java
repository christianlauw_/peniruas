package com.example.christianlauw.uas_penir_komikapp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class KomikActivity extends AppCompatActivity {
    Intent komik_intent;
    String komik_episode,komik_komik,komik_userid,komik_numEpFold, epName;
    static Integer flag=1;
    static  RecyclerView recyclerView;
    Dialog komik_dialog;
    FloatingActionButton komik_comment;
    static int statusLike=0;
    static FloatingActionButton fabLike;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_komik);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        komik_comment = (FloatingActionButton)findViewById(R.id.komik_comment);
        komik_intent = getIntent();
        komik_episode = String.valueOf(komik_intent.getIntExtra("PILIH_EPISODE_KOMIK_EPISODE_ID", 0));
        komik_komik = String.valueOf(komik_intent.getIntExtra("PILIH_EPISODE_KOMIK_KOMIK_ID", 0));
        komik_numEpFold = "0";
        komik_userid=Login.LOGIN_USERID;
        epName = komik_intent.getStringExtra("EPISODE_NAME");
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(epName);
        }



        //Toast.makeText(this, komik_episode + "," + komik_komik, Toast.LENGTH_SHORT).show();

        recyclerView = (RecyclerView) findViewById(R.id.komik_recycle);
        String[] field = {"ep_id","komik_id"};
        String[] value={komik_episode,komik_komik};
        SendPostRequest r = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/jumEpisode_komik.php",field,value,KomikActivity.this,flag,komik_numEpFold);
        r.execute();

        komik_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                komik_dialog=new Dialog(KomikActivity.this);
//                komik_dialog.setContentView(R.layout.comment_layout);
//                komik_dialog.setTitle("Comment");
//
//                final EditText comment_comment = (EditText)komik_dialog.findViewById(R.id.comment_content);
//                Button comment_submit = (Button)komik_dialog.findViewById(R.id.comment_submit);
//                ImageButton comment_back = (ImageButton)komik_dialog.findViewById(R.id.comment_back);
//                comment_submit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String[]field={"content","episode_id","user_id"};
//                        String[]values={comment_comment.getText().toString(),komik_episode.toString(),komik_userid.toString()};
//                        SendPostRequestAldo r = new SendPostRequestAldo("http://103.52.146.34/penir/penir12/penir12/scripts/insert_comment.php",field,values,KomikActivity.this,null,null,KomikActivity.this,null);
//                        r.execute();
//                    }
//                });
//                comment_back.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        komik_dialog.cancel();
//                    }
//                });
//                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                lp.copyFrom(komik_dialog.getWindow().getAttributes());
//                lp.width= WindowManager.LayoutParams.MATCH_PARENT;
//                lp.height=700;
//                komik_dialog.show();
//                komik_dialog.getWindow().setAttributes(lp);
                Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
                //Toast.makeText(KomikActivity.this, komik_episode, Toast.LENGTH_SHORT).show();
                intent.putExtra("episode_id", komik_episode);
                startActivity(intent);
            }
        });
        fabLike = (FloatingActionButton) findViewById(R.id.komik_Like);
        fabLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] field={"user_id","episode_id"};
                String[] value={komik_userid,komik_episode};
                if(statusLike==1)
                {
                    SendPostRequest sd = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_unlike_ep.php", field, value, getApplicationContext(), 2);
                    sd.execute();
                    Snackbar.make(view, "Deleted from my like", Snackbar.LENGTH_SHORT).show();
                }
                else {
                    SendPostRequest sd = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_like_ep.php", field, value, getApplicationContext(), 2);
                    sd.execute();
                    Snackbar.make(view, "Added to my like", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void postFinish(String result){
        if(result.equals("1")){
            Toast.makeText(this, "Comment Telah dibuat", Toast.LENGTH_SHORT).show();
            finish();
        }
        else{
            Toast.makeText(this, "Whoops, Something wrong occurred in system.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void setFABLikeStatus(String result)
    {
        if(result.equals("1"))
        {
            fabLike.setImageResource(R.drawable.ic_like_red_24dp);
            statusLike=1;
        }
        else
        {
            fabLike.setImageResource(R.drawable.ic_like_border_red_24dp);
            statusLike=0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }
}
