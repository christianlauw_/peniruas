package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Christian Lauw on 11/24/2017.
 */

public class RecycleViewHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public ArrayList<Komik> allKomik;
    public ArrayList<Product> allProduct;
    public Context con;

    public RecycleViewHomeAdapter(Context pCon,ArrayList<Komik> pAllKomik)
    {
        allKomik = new ArrayList<>();
        allKomik=pAllKomik;
        con = pCon;
        //Log.d("Masuk RecyHomeAdap","Success");
    }
//    public RecycleViewHomeAdapter(Context pCon,ArrayList<Product> pAllProduct)
//    {
//        allProduct = new ArrayList<>();
//        allProduct = pAllProduct;
//        con = pCon;
//        //Log.d("Masuk RecyHomeAdap","Success");
//    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_card_view,parent,false);
        RecyclerView.ViewHolder vHold = new RecyclerView.ViewHolder(v) {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        //Log.d("Masuk CreateViewAdap","Success");
        return vHold;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final TextView txtJudul = holder.itemView.findViewById(R.id.txtJudul);
        final TextView txtID = holder.itemView.findViewById(R.id.txtID);
        final TextView txtRating = holder.itemView.findViewById(R.id.txtRating);
        ImageView imgSet = holder.itemView.findViewById(R.id.imgCover);

        txtJudul.setText(allKomik.get(position).getJudul());
        txtID.setText(String.valueOf(allKomik.get(position).getId()));
        txtRating.setText(String.valueOf(allKomik.get(position).getRating()));


        ImageDownloader img = new ImageDownloader(imgSet);
        img.execute("http://103.52.146.34/penir/penir12/penir12/komik/"+txtID.getText()+"/cover.jpg");
        //txtJudul.setText(allProduct.get(position).getNama());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(con,PilihEpisodeKomikActivity.class);
                i.putExtra("judul_komik", txtJudul.getText().toString());
                i.putExtra("id_komik",txtID.getText().toString());
                i.putExtra("rating", txtRating.getText().toString());
                //Toast.makeText(con, txtID.getText().toString(), Toast.LENGTH_SHORT).show();
                con.startActivity(i);
            }
        });
        

    }

    @Override
    public int getItemCount() {
        return allKomik.size();
    }
}
