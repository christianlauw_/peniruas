package com.example.christianlauw.uas_penir_komikapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class GridKomikFragment extends Fragment {

    GridKomikAdapter adapter;
    public GridKomikFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_grid_komik, container, false);
        ArrayList<Komik> komiksToShow = getArguments().getParcelableArrayList("komiks");

        if(komiksToShow != null){
            adapter = new GridKomikAdapter(komiksToShow, this.getContext());
            GridView g = v.findViewById(R.id.grid);
            g.setAdapter(adapter);


            g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                    Intent intent = new Intent(getContext(),PilihEpisodeKomikActivity.class);
                    intent.putExtra("judul_komik", ((Komik)adapter.getItem(i)).getJudul());
                    intent.putExtra("id_komik",String.valueOf(id));
                    intent.putExtra("rating", String.valueOf(((Komik)adapter.getItem(i)).getRating()));
                    //Toast.makeText(con, txtID.getText().toString(), Toast.LENGTH_SHORT).show();
                    getContext().startActivity(intent);
                    //Toast.makeText(getContext(), String.valueOf(id), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            Toast.makeText(this.getContext(), "No data received", Toast.LENGTH_SHORT).show();
        }

        return v;
    }

}
