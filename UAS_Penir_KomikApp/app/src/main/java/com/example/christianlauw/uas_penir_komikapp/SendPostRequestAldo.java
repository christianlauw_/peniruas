package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Laptop-SI on 13/10/2017.
 */

public class SendPostRequestAldo extends AsyncTask<String, Void, String> {
    String sUrl;
    String[] field,values;
    Context context;
    Login login;
    Register register;
    KomikActivity komikActivity;
    EpKomikAdapter epKomikAdapter;
    public SendPostRequestAldo(String sUrl, String[] field, String[] values, Context context, Login login, Register register,KomikActivity komik, EpKomikAdapter ep) {
        this.sUrl = sUrl;
        this.field = field;
        this.values = values;
        this.context = context;
        this.epKomikAdapter=ep;
        this.login = login;
        this.register = register;
        this.komikActivity=komik;
    }

    protected void onPreExecute(){}

    protected String doInBackground(String... arg0) {
        try {

            URL url = new URL(sUrl); // here is your URL path

            JSONObject postDataParams = new JSONObject();
            for(int i=0;i<field.length;i++){
                postDataParams.put(field[i],values[i]);
            }

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in=new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream()));
                StringBuffer sb = new StringBuffer("");
                String line="";

                while((line = in.readLine()) != null) {

                    sb.append(line);
                    break;
                }

                in.close();
                return sb.toString();

            }
            else {
                return new String("false : "+responseCode);
            }
        }
        catch(Exception e){
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        //Toast.makeText(context , result,Toast.LENGTH_LONG).show();
        if(this.register == null && this.komikActivity==null && this.epKomikAdapter==null){
            login.postFinish(result);
        }
        else if(this.login==null && this.komikActivity==null&& this.epKomikAdapter==null){
            register.postFinish(result);
        }
        else if(this.login==null && this.register==null&& this.epKomikAdapter==null){
            komikActivity.postFinish(result);
        }
        else if(this.login==null && this.register==null && this.komikActivity==null){
           // epKomikAdapter.postFinish(result);
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

}

