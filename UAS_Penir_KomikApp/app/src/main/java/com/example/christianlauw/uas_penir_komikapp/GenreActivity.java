package com.example.christianlauw.uas_penir_komikapp;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;

public class GenreActivity extends AppCompatActivity {
    ArrayList<Fragment> fragments;
    TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);

        Toolbar toolbar =(Toolbar)findViewById(R.id.toolbarGenre);
        setSupportActionBar(toolbar);

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }

        fragments = new ArrayList<>();
        tabLayout = (TabLayout)findViewById(R.id.tabsGenre);

        ArrayList<ArrayList<Komik>> listKomikByGenre = new ArrayList<>();
        listKomikByGenre = GenreFragment.listGenreKomik;

        for(ArrayList<Komik> komiksGenre : listKomikByGenre){
            GridKomikFragment fragment = new GridKomikFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("komiks", komiksGenre);
            fragment.setArguments(bundle);
            fragments.add(fragment);
        }

        ViewPager vp = (ViewPager)findViewById(R.id.viewpagerGenre);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tbNew = tabLayout.getTabAt(position);
                tbNew.select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupViewPager(vp, fragments);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }

    public void setupViewPager(ViewPager viewPager, ArrayList<Fragment> fragments){
        FragmentPageAdapt adapter = new FragmentPageAdapt(getSupportFragmentManager());
        for(Fragment f : fragments){
            adapter.addFragment(f);
            Log.d("setup:",f.toString());
        }
        Log.d("Masuk","masuk setViewPager");
        viewPager.setAdapter(adapter);
    }
}
