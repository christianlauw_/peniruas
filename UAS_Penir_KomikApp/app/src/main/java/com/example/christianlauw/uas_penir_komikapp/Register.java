package com.example.christianlauw.uas_penir_komikapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Register extends AppCompatActivity {

    EditText register_username,register_password,register_email;
    ImageButton register_back;
    Button register_register;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        register_username=(EditText)findViewById(R.id.register_username);
        register_password=(EditText)findViewById(R.id.register_password);
        register_email=(EditText)findViewById(R.id.register_email);
        register_back=(ImageButton)findViewById(R.id.register_back);
        register_register=(Button)findViewById(R.id.register_daftar);

        register_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (register_username.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Username Tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else if (register_password.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Password Tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else if (register_email.getText().toString().equals("")) {
                    Toast.makeText(Register.this, "Email Tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else
                    try {
                        //tambahan saja, nanti di cek apakah username ada atau tidak
//                    String[]field={"username"};
//                    String[]values={register_username.getText().toString()};
//                    SendPostRequest r = new SendPostRequest("http://103.52.146.34/penir/penir12/penir12/scripts/user_user.php",field,values,Register.this);
//                    r.execute();
//                    if(data tidak ada jalankan bawah)

                        String[] field = {"username", "password", "email"};
                        String[] values = {register_username.getText().toString(), register_password.getText().toString(), register_email.getText().toString()};
                        SendPostRequestAldo r = new SendPostRequestAldo("http://103.52.146.34/penir/penir12/penir12/scripts/user_register.php", field, values, Register.this, null, Register.this, null, null);
                        r.execute();
                    } catch (Exception e) {
                        Toast.makeText(Register.this, "Error Register : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
            }
        });
        register_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        
    }

    public void postFinish(String result){
        if(result.equals("1")){
            Intent intent = new Intent(getApplicationContext(), Login.class);
            startActivity(intent);
            Toast.makeText(this, "Yay you have been registered. Please log in.", Toast.LENGTH_SHORT).show();
            finish();
        }
        else{
            Toast.makeText(this, "Whoops, Something wrong occurred in system.", Toast.LENGTH_SHORT).show();
        }
    }

}
