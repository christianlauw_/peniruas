package com.example.christianlauw.uas_penir_komikapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class SearchFragment extends Fragment {
    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_genre, container, false);
        ArrayList<Komik> listKomik = getArguments().getParcelableArrayList("komiks");
        GridKomikAdapter adapter = new GridKomikAdapter(listKomik, this.getContext());
        return v;
    }
}
